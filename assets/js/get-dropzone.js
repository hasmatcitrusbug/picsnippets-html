$(function(){

    var previewNode = document.querySelector("#view-drop-area");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);

    Dropzone.options.myAwesomeDropzone = {
      maxFilesize: 5,
      addRemoveLinks: true,
      dictResponseError: 'Server not Configured',
      acceptedFiles: ".mp4,.m4a,.m4v,.f4v,.f4a,.m4b,.mov,.3gp,.ogg,.wmv,.mkv,.png,.jpg",
      previewsContainer: "#previews", 
      init:function(){
        var self = this;
        // config
        self.options.addRemoveLinks = true;
        self.options.dictRemoveFile = "<i class='delete-icon delete-remove-icon'></i>";
        //New file added
        self.on("addedfile", function (file) {
          console.log('new file added ', file);
        });
        // Send file starts
        self.on("sending", function (file) {
          console.log('upload started', file);
          $('.meter').show();
        });
        
        // File upload Progress
        self.on("totaluploadprogress", function (progress) {
          console.log("progress ", progress);
          $('.roller').width(progress + '%');
        });
  
        self.on("queuecomplete", function (progress) {
          $('.meter').delay(999).slideUp(999);
        });
        
        // On removing file
        self.on("removedfile", function (file) {
          console.log(file);
        });
      }
    };

    Dropzone.options.myAwesomeDropzone2 = {
      maxFilesize: 5,
      addRemoveLinks: true,
      dictResponseError: 'Server not Configured',
      acceptedFiles: ".mp4,.m4a,.m4v,.f4v,.f4a,.m4b,.mov,.3gp,.ogg,.wmv,.mkv,.png,.jpg",
      previewsContainer: "#previews-2", 
      init:function(){
        var self = this;
        // config
        self.options.addRemoveLinks = true;
        self.options.dictRemoveFile = "<i class='delete-icon delete-remove-icon'></i>";
        //New file added
        self.on("addedfile", function (file) {
          console.log('new file added ', file);
          $("#previews-2 div.dz-preview" ).removeClass('dz-preview-border');
          $("#previews-2 div.dz-preview:last-child" ).addClass('dz-preview-border');


          
        });
        // Send file starts
        self.on("sending", function (file) {
          console.log('upload started', file);
          $('.meter').show();
        });
        
        // File upload Progress
        self.on("totaluploadprogress", function (progress) {
          console.log("progress ", progress);
          $('.roller').width(progress + '%');
        });
  
        self.on("queuecomplete", function (progress) {
          $('.meter').delay(999).slideUp(999);
        });
        
        // On removing file
        self.on("removedfile", function (file) {
          console.log(file);
        });
      }
    };

    Dropzone.options.myAwesomeDropzone3 = {
      maxFilesize: 5,
      addRemoveLinks: true,
      dictResponseError: 'Server not Configured',
      acceptedFiles: ".mp4,.m4a,.m4v,.f4v,.f4a,.m4b,.mov,.3gp,.ogg,.wmv,.mkv,.png,.jpg",
      previewsContainer: "#previews-3", 
      init:function(){
        var self = this;
        // config
        self.options.addRemoveLinks = true;
        self.options.dictRemoveFile = "<i class='delete-icon delete-remove-icon'></i>";
        //New file added
        self.on("addedfile", function (file) {
          console.log('new file added ', file);
          $("#previews-3 div.dz-preview" ).removeClass('dz-preview-border');
          $("#previews-3 div.dz-preview:last-child" ).addClass('dz-preview-border');

          $('.dz-size').text('Uploaded Audio');

          
        });
        // Send file starts
        self.on("sending", function (file) {
          console.log('upload started', file);
          $('.meter').show();
        });
        
        // File upload Progress
        self.on("totaluploadprogress", function (progress) {
          console.log("progress ", progress);
          $('.roller').width(progress + '%');
        });
  
        self.on("queuecomplete", function (progress) {
          $('.meter').delay(999).slideUp(999);
        });
        
        // On removing file
        self.on("removedfile", function (file) {
          console.log(file);
        
        });
      }
    };

  })

  // /////

